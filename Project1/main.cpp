#include <iostream>

int main()
{
    std::string Name = "Hello Skillbox";
    
    std::cout << Name << "\n";
    
    //std::cout << Name.capacity() << "\n";

    std::cout << "Length string is " << Name.length() << "\n";

    std::cout << "Char in begin string is " << Name.at(0) << "\n";

    std::cout << "Char in end string is " << Name.at(Name.length()-1) << "\n";

    return 0;
}